from faiss_utils import *


class BingemIndex:

    def __init__(self, path=None) -> None:
        self.idx = faiss.read_index(path) if path else None
        self.use_gpu = False

    def build_index(self, xb: np.ndarray, param="IDMap,Flat", ids=None, use_gpu=False, path=None):
        print("[Bingem Debug]    build index ...")
        last = time.time()
        dim = xb.shape[1]
        nb = xb.shape[0]
        if not ids:
            ids = np.arange(0, nb).astype(np.int64)
        elif ids.dtype == np.int64:
            print("illegal ids type, np.int64 is required!")
        measure = faiss.METRIC_L2
        self.idx = faiss.index_factory(dim, param, measure)
        if use_gpu:
            self.cpu_to_gpu()
        else:
            self.gpu_to_cpu()
        self.idx.train(xb)
        self.idx.add_with_ids(xb, ids)  # add vectors to the index
        print("[Bingem Debug]    build index finish, time elaspsed: {}s".format(time.time() - last))
        if path:
            faiss.write_index(self.idx, path)
            print("[Bingem Debug]    index saved in {}".format(path))

    def cpu_to_gpu(self):
        if not self.use_gpu:
            print("[Bingem Debug]    cpu_to_gpu ...")
            last = time.time()
            co = faiss.GpuClonerOptions()
            co.useFloat16 = True
            res = faiss.StandardGpuResources()
            self.idx = faiss.index_cpu_to_gpu(res, 0, self.idx, co)
            self.use_gpu = True
            print("[Bingem Debug]    cpu_to_gpu finish, time elaspsed: {}s".format(time.time() - last))

    def gpu_to_cpu(self):
        if self.use_gpu:
            print("[Bingem Debug]    gpu_to_cpu ...")
            last = time.time()
            self.idx = faiss.index_gpu_to_cpu(self.idx)  ## cpu index can not be saved, so gpu2cpu before saving index
            self.use_gpu = False
            print("[Bingem Debug]    gpu_to_cpu finish, time elaspsed: {}s".format(time.time() - last))

    def search_topk(self, xq, topk):
        if not self.idx:
            print("index is not exists!")
        print("[Bingem Debug]    search ...")
        last = time.time()
        D, I = self.idx.search(xq, topk)  # actual search
        print("[Bingem Debug]    search fini, time elaspsed: {}s".format(time.time() - last))
        return D, I

        # def search_by_id(self, id):
    #     return self.idx.xb.at(id)


def test_performance(n_test, file_path=None):
    file_ctrl = open(file_path, "w") if file_path else None
    save_and_print(file_ctrl, "[Large scale KNN experiment]")
    save_and_print(file_ctrl,
                   "[Setting introductions]\n\tn_b: {}, \n\tn_q: {}, \n\ttopk: {}, \n\tdim: {}, \n\tuse_gpu: {}\n".format(
                       "vector lib scale", "query vector scale", "recall@topk", "dimension of vector",
                       "nvidia rtx3090 use_or_not"))
    nb_settings = [200_000, 2_000_000, 20_000_000]
    nq_settings = [1, 32, 1024]
    k_settings = [64, 256]
    gpu_settings = [True]
    d = 32
    save_and_print(file_ctrl, "[Bingem Debug]    Results")
    for nb in nb_settings:
        for use_gpu in gpu_settings:
            xb = init_vectors(n=nb, d=d)
            bi = BingemIndex()
            index_type = 'IDMap,IVF64_HNSW32,PQ8'
            bi.build_index(xb=xb, param=index_type, use_gpu=use_gpu)
            for nq in nq_settings:
                xq = init_vectors(n=nb, d=d)
                for k in k_settings:
                    save_and_print(file_ctrl,
                                   "::  Settings: n_b:{n_b}, n_q:{n_q}, topk:{topk}, dim:{dim}, use_gpu:{gpu}"
                                   .format(n_b=nb, n_q=nq, topk=k, dim=d, gpu=use_gpu))
                    last_time = time.time()
                    for i in range(n_test):
                        bi.search_topk(xq, topk=k)
                    time_elaspse = (time.time() - last_time) / n_test
                    save_and_print(file_ctrl, "::  Time cost: {}s \n".format("{:-.4f}".format(time_elaspse)))
                    if file_ctrl:
                        file_ctrl.flush()
    if file_path:
        file_ctrl.close()


if __name__ == "__main__":
    path = "./experiment_results.txt"
    test_performance(n_test=4, file_path=path)

