import time
import numpy as np
import faiss  # make faiss available
from faiss import write_index


def gen_data(nb, nq, d):
    np.random.seed(1234)  # make reproducible
    xb = np.random.random((nb, d)).astype('float32')
    xb[:, 0] += np.arange(nb) / 1000.
    xq = np.random.random((nq, d)).astype('float32')
    xq[:, 0] += np.arange(nq) / 1000.
    return xb, xq


def build_idx_with_factory(xb, index_param='FlatL2', ids=None, gpu_support=True):
    d = xb.shape[1]
    measure = faiss.METRIC_L2
    param = index_param
    idx = faiss.index_factory(d, param, measure)
    if gpu_support:
        res = faiss.StandardGpuResources()  # use a single GPU
        idx = faiss.index_cpu_to_gpu(res, 0, idx)
    idx.train(xb)
    if ids is None:
        idx.add(xb)  # add vectors to the index
    else:
        idx.add_with_ids(xb, ids)
    print("Total vecors: ", idx.ntotal)
    return idx


def build_idx_flatl2(xb, ids=None):
    d = xb.shape[1]
    res = faiss.StandardGpuResources()
    cfg = faiss.GpuIndexFlatConfig()
    cfg.useFloat16 = True

    index_flat = faiss.IndexFlatIP(d)
    idx_with_id = faiss.IndexIDMap(index_flat)
    gpu_index_flat = faiss.index_cpu_to_gpu(res, 0, idx_with_id)
    # idx_with_id = faiss.IndexIDMap(gpu_index_flat)
    gpu_index_flat.add_with_ids(xb, ids)
    print("Total vecors: ", gpu_index_flat.ntotal)
    return gpu_index_flat


def search(index, q, k):
    D, I = index.search(q, k)  # sanity check
    return D, I


def save(index, path):
    '''
    only cpu index can be saved


    index_flat = faiss.index_gpu_to_cpu(index_flat)
    :param index:
    :param path:
    :return:
    '''
    write_index(index, path)


def load(path):
    return faiss.read_index(path)

def init_vectors(n=2_000, d=32, seed=0):
    print("[Debug Info]  init vectors ...")
    last = time.time()
    x_vec = np.random.random((n, d)).astype('float32')
    x_vec[:, 0] += np.arange(n) / 1000.
    print("[Debug Info]  init vectors fini, time elaspsed: {}s".format(time.time() - last))
    return x_vec


def save_and_print(file_control, ln):
    if file_control:
        file_control.write(ln + "\n")
    print(ln)

if __name__ == '__main__':
    nb,nq = 10_000, 1
    d = 16
    xb, xq = gen_data(nb, nq, d)
    ids = np.arange(0, nb).astype(np.int64)
    idx = build_idx_flatl2(xb, ids)
    # save(idx, "flat.index")